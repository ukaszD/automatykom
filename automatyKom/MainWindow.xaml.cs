﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace gameOfLife
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int iteration;
        int N;
        Thread mainThread;
        Engine engine;
        bool stopFlag;
        double speed;
        int cellSize;
        int threadCunter;
        int meshSize_X;

        public MainWindow()
        {
            InitializeComponent();
            iteration = Int32.Parse(textboxIter.Text);
            meshSize_X = Int32.Parse(meshSizeN.Text);
            stopFlag = false;
            speed = Double.Parse(textboxSpeed.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            mainThread = new Thread(threadMethod);
            N = (int)meshSize_X;
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;
            engine = new Engine(cGameField);
            engine.firstCount(N, cellSize, comboBoxType.SelectedIndex);
            drawMesh();
            labelRad.Visibility = Visibility.Hidden;
            sliderRad.Visibility = Visibility.Hidden;
            labelX.Visibility = Visibility.Hidden;
            labelY.Visibility = Visibility.Hidden;
            sliderX.Visibility = Visibility.Hidden;
            sliderY.Visibility = Visibility.Hidden;
            labelRand.Visibility = Visibility.Hidden;
            sliderRand.Visibility = Visibility.Hidden;
            threadCunter = 0;
            Closing += this.OnWindowClosing;
        }

        private void resetEngine()
        {
            iteration = Int32.Parse(textboxIter.Text);
            cellSize = Int32.Parse(comboBoxSize.Text);
            if (meshSizeN != null && meshSizeN.Text != null)
                meshSize_X = Int32.Parse(meshSizeN.Text);
            speed = Double.Parse(textboxSpeed.Text);

            N = (int)meshSize_X;
           
            if (labelMeshSize != null)
                labelMeshSize.Content = N.ToString() + "x" + N.ToString();
            bool cGameField = dbCircularGameField.IsChecked == true ? true : false;
            engine = new Engine(cGameField);
            //stopFlag = true;
            iterationCountLabel.Content = "0";
            threadCunter = 0;
            // mainThread = new Thread(threadMethod);
            engine.firstCount(N, cellSize, comboBoxType.SelectedIndex);
            drawMesh();
        }

        private void drawMesh()
        {
            canvas.Children.Clear();
            for (int j = 0; j < N; j++)
            {
                for (int i = 0; i < N; i++)
                {
                    Canvas.SetLeft(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionX);
                    Canvas.SetTop(engine.cells[i, j].body, engine.cells[i, j].size * engine.cells[i, j].positionY);
                    canvas.Children.Add(engine.cells[i, j].body);
                }
            }
        }

        private void redrawMesh(int N)
        {
            engine.count(N);
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            //stopFlag = false;
            iteration = Int32.Parse(textboxIter.Text);
            if (mainThread.ThreadState == System.Threading.ThreadState.Unstarted)
                mainThread.Start();
            else if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();

        }

        private void resetBtn_Click(object sender, RoutedEventArgs e)
        {
            resetEngine();
        }

        private void stopBtn_Click(object sender, RoutedEventArgs e)
        {
            //stopFlag = true;
            if (mainThread.ThreadState != System.Threading.ThreadState.Unstarted)
                mainThread.Suspend();
        }

        private void threadMethod()
        {
            while (threadCunter <= iteration && !stopFlag)
            {
                Thread.Sleep(TimeSpan.FromSeconds(speed));
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    redrawMesh(N);
                    iterationCountLabel.Content = threadCunter.ToString();
                    threadCunter++;
                    //Console.WriteLine("Working iter: " + threadCunter);
                });
            }
            Console.WriteLine("Thread terminating gracefully.");
        }

        private void comboBoxZ_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (labelRad != null && sliderRad != null)
            {
                switch (comboBoxZ.SelectedIndex)
                {
                    case 0:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = "";
                            labelTopSlider.Content = "";
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                    case 1:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = sliderY.Value.ToString();
                            labelTopSlider.Content = sliderX.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Visible;
                        labelY.Visibility = Visibility.Visible;
                        sliderX.Visibility = Visibility.Visible;
                        sliderY.Visibility = Visibility.Visible;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                    case 2:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content ="";
                            labelTopSlider.Content = sliderRand.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Visible;
                        sliderRand.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        if (labelBottomSlider != null && labelTopSlider != null)
                        {
                            labelBottomSlider.Content = sliderRad.Value.ToString();
                            labelTopSlider.Content = sliderRand.Value.ToString();
                        }
                        labelRad.Visibility = Visibility.Visible;
                        sliderRad.Visibility = Visibility.Visible;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Visible;
                        sliderRand.Visibility = Visibility.Visible;
                        break;
                    default:
                        labelRad.Visibility = Visibility.Hidden;
                        sliderRad.Visibility = Visibility.Hidden;
                        labelX.Visibility = Visibility.Hidden;
                        labelY.Visibility = Visibility.Hidden;
                        sliderX.Visibility = Visibility.Hidden;
                        sliderY.Visibility = Visibility.Hidden;
                        labelRand.Visibility = Visibility.Hidden;
                        sliderRand.Visibility = Visibility.Hidden;
                        break;
                }
            }
        }

        private void comboBoxType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            resetEngine();
        }

        private void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            switch (comboBoxZ.SelectedIndex)
            {
                case 0:

                    break;
                case 1:
                    engine.uniformCellSelect((int)sliderX.Value, (int)sliderY.Value);
                    break;
                case 2:
                    engine.randomCellSelect(this.N, (int)sliderRand.Value);
                    break;
                case 3:
                    engine.radCellSelect((int)sliderRad.Value, (int)sliderRand.Value, this.N);
                    break;
            }
        }

        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            if (mainThread.ThreadState == System.Threading.ThreadState.Suspended)
                mainThread.Resume();
            stopFlag = true;
            Console.WriteLine("Program terminated");

        }

        private void sliderRad_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderRad.Visibility == Visibility.Visible && labelBottomSlider != null)
                labelBottomSlider.Content = sliderRad.Value.ToString();
        }

        private void sliderRand_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderRand.Visibility == Visibility.Visible && labelTopSlider != null)
                labelTopSlider.Content = sliderRand.Value.ToString();
        }

        private void sliderY_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderY.Visibility == Visibility.Visible && labelBottomSlider != null)
                labelBottomSlider.Content = sliderY.Value.ToString();
        }

        private void sliderX_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderX.Visibility == Visibility.Visible && labelTopSlider != null)
                labelTopSlider.Content = sliderX.Value.ToString();
        }

        private void meshSizeRB_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void cellSizeRB_Checked(object sender, RoutedEventArgs e)
        {

        }
    }
}
